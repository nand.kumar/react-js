import React from 'react'

const Props = (props) => {
  return (
      <>
    <h1>hello {props.name}</h1>
    <p>age is {props.age}</p>
      </>
  )
}

export default Props