import React from 'react'

const Navbar = () => {
  return (
    <div>
        <nav className="navbar">
            <h1>The Bitcs Blog</h1>
            <div className="links">
                <a href="/">About</a>
                <a href="/">Contact Us</a>
            </div>
        </nav>
    </div>
  )
}

export default Navbar