import './App.css';
import ClickEvent from './components/ClickEvent';
import Navbar from './components/Navbar';
import Props from './components/Props';
import WelcomeClass from './components/WelcomeClass';

function App() {
  const title="Welcome to react tutorial";
  const link="https://bitcs.in/"
  return (
    <>
    <Navbar/>
    <h1>hi there</h1>
    <p>{title}</p>
    <p>{10}</p>
    <p>{"nand kumar"}</p>

    <a href={link}>Click Here</a>
    <ClickEvent/>
    <WelcomeClass/>

    <Props name="nand" age={49}/>
    <Props name="nittin" age={49+20}/>
    <Props name="hurrr" age={49-20}/>
    </>
  );
}

export default App;
